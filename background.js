String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		if (request.command == 0) {
			var tabID = -1;
			var url = request.url;
			if (!(url.endsWith("random") || url.endsWith("random/")))
				if (url.endsWith("/"))
					url = url + "random";
				else
					url = url + "/random";

			chrome.tabs.create({active:true, url: url}, function(tab) {
		  		injectScripts(tab.id);
		  		var shuffles = 0;
		  		var i = setInterval(function() {
		  			if (shuffles >= request.shuffles)
		  				clearInterval(i);

			    	chrome.tabs.update(tab.id, {url: url});
			    	shuffles++;
			    }, request.interval*1000);

			    chrome.tabs.onRemoved.addListener(function callback(id, info) {
			    	if (id == tab.id) {
			    		chrome.tabs.onRemoved.removeListener(callback);
			    		
			    		clearInterval(i);
			    	}
			    });
		  	});
		}
});

function injectScripts(tabID) {
	chrome.tabs.executeScript(tabID, {
		file: 'jquery.min.js'
	});
	chrome.tabs.executeScript(tabID, {
		file: 'mouse_simulation.js'
	}); 
	chrome.tabs.executeScript(tabID, {
		file: 'inject.js'
	});
}