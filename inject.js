//alert(document.onmousemove);

function findLink(element) {
	var link = element.getElementsByTagName("a")[0];
	if (link != null)
		return link;

	var iframes = element.getElementsByTagName("iframe");

	for (var i = 0; i < iframes.length; i++) {
		var iframe = iframes[i];
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;

		var link = findLink(innerDoc);
		if (link != null)
			return link;
	}
	
	return null;
}

function getAdLink() {
	var iframe = document.getElementsByClassName("adsbygoogle")[0];

	return findLink(iframe);
}

function test() {
	alert(getAdLink());
	//document.write("");
}

getAdLink();